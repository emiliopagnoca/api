﻿using CadastroColaborador.Domain.Enums;
using CadastroColaborador.Domain.Handlers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace CadastroColaborador.Infra.Handlers
{
    public class HabilidadesHandler : SqlMapper.TypeHandler<List<Habilidades>>
    {
        public override List<Habilidades> Parse(object habilidades)
        {
            return GenericHandler.StringParaListaHabilidades(habilidades.ToString());
        }

        public override void SetValue(IDbDataParameter parameter, List<Habilidades> habilidades)
        {
            parameter.Value = habilidades.ToString();
        }
    }
}
