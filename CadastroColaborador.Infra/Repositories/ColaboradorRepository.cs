﻿using CadastroColaborador.Domain.Entities;
using CadastroColaborador.Domain.Enums;
using CadastroColaborador.Domain.Handlers;
using CadastroColaborador.Domain.Interfaces.Repositories;
using CadastroColaborador.Infra.Handlers;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroColaborador.Infra.Repositories
{
    public class ColaboradorRepository : IColaboradorRepository
    {
        private const string selecionarColaboradores = "SELECT Id, Nome, Nascimento, Email, Sexo, Habilidades, Ativo FROM Colaborador";
        private const string selecionarColaboradorPorId = "SELECT Id, Nome, Nascimento, Email, Sexo, Habilidades FROM Colaborador WHERE Id = @IdColaborador";
        private const string deletarColaborador = "DELETE FROM Colaborador WHERE Id = @IdColaborador";
        private const string inserirColaborador = "INSERT INTO Colaborador (Nome, Nascimento, Email, Sexo, Habilidades, Ativo) VALUES (@Nome, @Nascimento, @Email, @Sexo, @Habilidades, @Ativo)";
        private const string editarColaborador = "UPDATE Colaborador SET Nome = @Nome, Nascimento = @Nascimento, Email = @Email, Sexo = @Sexo, Habilidades = @Habilidades, Ativo = @Ativo WHERE Id = @Id";

        private IConfiguration configuration;

        public ColaboradorRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task CadastrarColaborador(ColaboradorEntity colaborador)
        {
            using (SqlConnection connect = new SqlConnection(configuration.GetConnectionString("FrameworkDatabase")))
            {
                try
                {
                    connect.Open();

                    await connect.QueryAsync<ColaboradorEntity>(inserirColaborador,
                        new
                        {
                            Nome = colaborador.Nome,
                            Nascimento = colaborador.Nascimento,
                            Email = colaborador.Email,
                            Sexo = colaborador.Sexo,
                            Habilidades = GenericHandler.ToString(colaborador.Habilidades),
                            Ativo = colaborador.Ativo
                        }, commandType: CommandType.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        public async Task DeletarColaborador(int id)
        {
            using (SqlConnection connect = new SqlConnection(configuration.GetConnectionString("FrameworkDatabase")))
            {
                try
                {
                    connect.Open();

                    await connect.QueryAsync<ColaboradorEntity>(deletarColaborador,
                        new { IdColaborador = id }, commandType: CommandType.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        public async Task EditarColaborador(ColaboradorEntity colaborador, int id)
        {
            using (SqlConnection connect = new SqlConnection(configuration.GetConnectionString("FrameworkDatabase")))
            {
                try
                {
                    connect.Open();

                    await connect.QueryAsync<ColaboradorEntity>(editarColaborador,
                        new
                        {
                            Id = id,
                            Nome = colaborador.Nome,
                            Nascimento = colaborador.Nascimento,
                            Email = colaborador.Email,
                            Sexo = colaborador.Sexo,
                            Habilidades = GenericHandler.ToString(colaborador.Habilidades),
                            Ativo = colaborador.Ativo
                        }, commandType: CommandType.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        public async Task<IEnumerable<ColaboradorEntity>> ListarColaboradores()
        {
            using (SqlConnection connect = new SqlConnection(configuration.GetConnectionString("FrameworkDatabase")))
            {
                try
                {
                    connect.Open();

                    SqlMapper.ResetTypeHandlers();
                    SqlMapper.AddTypeHandler(new HabilidadesHandler());

                    return await connect.QueryAsync<ColaboradorEntity>(selecionarColaboradores, commandType: CommandType.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }

        public async Task<ColaboradorEntity> RetornarColaboradorPorId(int id)
        {
            using (SqlConnection connect = new SqlConnection(configuration.GetConnectionString("FrameworkDatabase")))
            {
                try
                {
                    connect.Open();

                    SqlMapper.ResetTypeHandlers();
                    SqlMapper.AddTypeHandler(new HabilidadesHandler());

                    ColaboradorEntity colaborador = new ColaboradorEntity();

                    IEnumerable<ColaboradorEntity> colaboradores = await connect.QueryAsync<ColaboradorEntity>(selecionarColaboradorPorId,
                        new { IdColaborador = id }, commandType: CommandType.Text);

                    if(colaboradores.Count() > 0)
                        colaborador = colaboradores.First();

                    return colaborador;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connect.Close();
                }
            }
        }
    }
}
