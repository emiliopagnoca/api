USE [master]
GO
SET IDENTITY_INSERT [dbo].[Colaborador] ON 
GO
INSERT [dbo].[Colaborador] ([Id], [Nome], [Nascimento], [Email], [Sexo], [Habilidades], [Ativo]) VALUES (1, N'Emilio Pagnoca', CAST(N'2020-06-24T00:00:00.000' AS DateTime), N'emilio@email.com', 0, N'CSharp,SQL', 1)
GO
INSERT [dbo].[Colaborador] ([Id], [Nome], [Nascimento], [Email], [Sexo], [Habilidades], [Ativo]) VALUES (2, N'Maria da Silva', CAST(N'2020-06-23T20:04:30.507' AS DateTime), N'maria.silva@email.com', 1, N'2,4', 1)
GO
INSERT [dbo].[Colaborador] ([Id], [Nome], [Nascimento], [Email], [Sexo], [Habilidades], [Ativo]) VALUES (3, N'Mariana Fonseca', CAST(N'2020-06-23T20:04:50.400' AS DateTime), N'mariana.fonseca@email.com', 1, N'1,4', 1)
GO
INSERT [dbo].[Colaborador] ([Id], [Nome], [Nascimento], [Email], [Sexo], [Habilidades], [Ativo]) VALUES (4, N'Manoel Oliveira', CAST(N'2020-06-23T20:05:17.690' AS DateTime), N'manoel.oliveira@email.com', 0, N'1,2,3', 1)
GO
SET IDENTITY_INSERT [dbo].[Colaborador] OFF
GO
