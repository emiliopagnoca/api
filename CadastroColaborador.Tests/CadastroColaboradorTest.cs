using CadastroColaborador.API.Controllers;
using CadastroColaborador.Domain.Entities;
using CadastroColaborador.Domain.Enums;
using CadastroColaborador.Domain.Interfaces.Repositories;
using CadastroColaborador.Domain.Interfaces.Services;
using CadastroColaborador.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace CadastroColaborador.Tests
{
    public class CadastroColaboradorTest
    {
        private ColaboradorController controller;
        private Mock<IColaboradorService> colaboradorServiceMock;
        private Mock<IColaboradorRepository> colaboradorRepositoryMock;
        private ColaboradorEntity colaboradorEntityMock;

        [Fact]
        public void CadastrarColaborador_DeveRetornarOk()
        {
            //ARRANGE
            colaboradorEntityMock = new ColaboradorEntity()
            {
                Nome = "Teste teste",
                Email = "teste@email.com",
                Nascimento = new DateTime(1988, 10, 10),
                Sexo = Sexo.Masculino,
                Habilidades = new List<Habilidades>() { Habilidades.Angular },
                Ativo = true
            };

            colaboradorRepositoryMock = new Mock<IColaboradorRepository>();
            colaboradorRepositoryMock.Setup(x => x.CadastrarColaborador(colaboradorEntityMock));
            colaboradorServiceMock = new Mock<IColaboradorService>();
            colaboradorServiceMock.Setup(x => x.CadastrarColaborador(colaboradorEntityMock));
            controller = new ColaboradorController(colaboradorServiceMock.Object);


            //ACT
            Task<IActionResult> resultado = controller.Post(colaboradorEntityMock);

            //ASSERT
            Assert.NotNull(resultado);
            Assert.IsAssignableFrom<Task>(resultado);
        }
    }
}
