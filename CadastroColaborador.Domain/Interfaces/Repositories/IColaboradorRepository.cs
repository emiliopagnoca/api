﻿using CadastroColaborador.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CadastroColaborador.Domain.Interfaces.Repositories
{
    public interface IColaboradorRepository
    {
        Task CadastrarColaborador(ColaboradorEntity colaborador);
        Task EditarColaborador(ColaboradorEntity colaborador, int id);
        Task DeletarColaborador(int id);
        Task<IEnumerable<ColaboradorEntity>> ListarColaboradores();
        Task<ColaboradorEntity> RetornarColaboradorPorId(int id);
    }
}
