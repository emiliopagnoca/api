﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CadastroColaborador.Domain.Enums
{
    public enum Sexo
    {
        Masculino = 0,
        Feminino = 1
    }
}
