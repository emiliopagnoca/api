﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CadastroColaborador.Domain.Enums
{
    public enum Habilidades
    {
        CSharp = 0,
        Java = 1,
        Angular = 2,
        SQL = 3,
        ASP = 4
    }
}
