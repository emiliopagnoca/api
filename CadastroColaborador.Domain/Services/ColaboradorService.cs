﻿using CadastroColaborador.Domain.Entities;
using CadastroColaborador.Domain.Interfaces.Repositories;
using CadastroColaborador.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CadastroColaborador.Domain.Services
{
    public class ColaboradorService : IColaboradorService
    {
        private readonly IColaboradorRepository colaboradorRepository;

        public ColaboradorService(IColaboradorRepository colaboradorRepository)
        {
            this.colaboradorRepository = colaboradorRepository;
        }

        public async Task CadastrarColaborador(ColaboradorEntity colaborador)
        {
            await this.colaboradorRepository.CadastrarColaborador(colaborador);
        }

        public async Task DeletarColaborador(int id)
        {
            await this.colaboradorRepository.DeletarColaborador(id);
        }

        public async Task EditarColaborador(ColaboradorEntity colaborador, int id)
        {
            await this.colaboradorRepository.EditarColaborador(colaborador, id);
        }

        public async Task<IEnumerable<ColaboradorEntity>> ListarColaboradores()
        {
            return await this.colaboradorRepository.ListarColaboradores();
        }

        public async Task<ColaboradorEntity> RetornarColaboradorPorId(int id)
        {
            return await this.colaboradorRepository.RetornarColaboradorPorId(id);
        }
    }
}
