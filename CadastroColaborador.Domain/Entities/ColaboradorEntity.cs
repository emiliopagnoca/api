﻿using CadastroColaborador.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CadastroColaborador.Domain.Entities
{
    public class ColaboradorEntity
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo {0} é obrigatório.")]
        [RegularExpression("^([a-zA-Z\\-ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïòóôõöùúûüýÿ']{2,75} ?){2,5}$", ErrorMessage = "Nome e sobrenome são obrigatórios.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo {0} é obrigatório.")]
        [DataType(DataType.Date)]
        public DateTime Nascimento { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail inválido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo {0} é obrigatório.")]
        public Sexo Sexo { get; set; }

        [Required(ErrorMessage = "O preenchimento do campo {0} é obrigatório.")]
        public List<Habilidades> Habilidades { get; set; }

        public bool Ativo { get; set; }
    }
}
