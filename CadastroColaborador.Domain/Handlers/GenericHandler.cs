﻿using CadastroColaborador.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadastroColaborador.Domain.Handlers
{
    public class GenericHandler
    {
        public static string ToString(List<Habilidades> habilidades)
        {
            return string.Join(",", habilidades);
        }

        public static List<Habilidades> StringParaListaHabilidades(string value)
        {
            List<Habilidades> resultado = new List<Habilidades>();

            string[] habilidades = value.ToString().Split(',');

            resultado.AddRange(habilidades.Select(x => (Habilidades)Enum.Parse(typeof(Habilidades), x, true)));

            return resultado;
        }
    }

    
}
