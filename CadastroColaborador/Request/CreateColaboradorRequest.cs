﻿using CadastroColaborador.Domain.Enums;
using System;
using System.Collections.Generic;

namespace CadastroColaborador.API.Request
{
    public class CreateColaboradorRequest
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime Nascimento { get; set; }
        public string Email { get; set; }
        public Sexo Sexo { get; set; }
        public List<Habilidades> Habilidades { get; set; }
        public bool Ativo { get; set; }
    }
}
