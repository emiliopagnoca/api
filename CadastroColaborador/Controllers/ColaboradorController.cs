﻿using CadastroColaborador.API.App;
using CadastroColaborador.API.Request;
using CadastroColaborador.Domain.Entities;
using CadastroColaborador.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CadastroColaborador.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColaboradorController : ControllerBase
    {
        private readonly IColaboradorService colaboradorService;

        public ColaboradorController(IColaboradorService colaboradorService)
        {
            this.colaboradorService = colaboradorService;
        }

        // GET: api/<ColaboradorController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await this.colaboradorService.ListarColaboradores();
                return Ok(result);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }

        // GET api/<ColaboradorController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                if (id < 0)
                    return BadRequest("Id inválido.");

                var resultado = await this.colaboradorService.RetornarColaboradorPorId(id);

                if (string.IsNullOrEmpty(resultado.Nome))
                    return NotFound("O colaborador informado não foi encontrado.");

                return Ok(resultado);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }

        // POST api/<ColaboradorController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateColaboradorRequest colaboradorRequest)
        {
            try
            {
                var colaborador = RequestMapper.RealizarMapper(colaboradorRequest);

                if (!ModelState.IsValid)
                    return BadRequest();

                await this.colaboradorService.CadastrarColaborador(colaborador);
                return Ok();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }

        // PUT api/<ColaboradorController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CreateColaboradorRequest colaboradorRequest)
        {
            try
            {
                var colaborador = RequestMapper.RealizarMapper(colaboradorRequest);

                if (!ModelState.IsValid)
                    return BadRequest();

                await this.colaboradorService.EditarColaborador(colaborador, id);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }

        // DELETE api/<ColaboradorController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.colaboradorService.DeletarColaborador(id);
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500, "Ocorreu um erro interno.");
            }
        }
    }
}
