﻿using CadastroColaborador.API.Request;
using CadastroColaborador.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadastroColaborador.API.App
{
    public class RequestMapper
    {
        public static ColaboradorEntity RealizarMapper(CreateColaboradorRequest colaborador)
        {
            ColaboradorEntity colaboradorEntity = new ColaboradorEntity()
            {
                Id = colaborador.Id,
                Nome = colaborador.Nome,
                Email = colaborador.Email,
                Nascimento = colaborador.Nascimento,
                Sexo = colaborador.Sexo,
                Habilidades = colaborador.Habilidades,
                Ativo = colaborador.Ativo
            };

            return colaboradorEntity;
        }
    }
}
